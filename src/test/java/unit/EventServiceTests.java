package unit;


import calendar.entities.event.CustomEvent;
import calendar.entities.event.Event;
import calendar.entities.event.SystemEvent;
import calendar.entities.event.exceptions.EventException;
import calendar.services.CustomEventService;
import calendar.services.SystemEventService;
import calendar.services.impl.EventServiceImpl;
import calendar.transports.EventTransport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class EventServiceTests {

    @Mock
    SystemEventService systemEventService;
    @Mock
    CustomEventService customEventService;

    @InjectMocks
    EventServiceImpl eventService;


    @Test
    public void findEvents_GivenOnlySchoolId_ReturnsAllEventsBelongingToThatSchool() throws EventException {
        CustomEvent customEvent = new CustomEvent();
        customEvent.setSchoolId("1");
        customEvent.setDate(new Date(System.currentTimeMillis()));
        customEvent.setId("c1");
        customEvent.setEvent(new Event("event1", "custom event",
                new Time(System.currentTimeMillis() - (1000 * 60 * 60 * 5)), new Time(System.currentTimeMillis() - (1000 * 60 * 60 * 4)
        )));
        customEvent.getEvent().setCustomEvent(customEvent);
        when(customEventService.findAllBySchoolId("1")).thenReturn(Collections.singletonList(customEvent));

        SystemEvent systemEvent = new SystemEvent();
        systemEvent.setSchoolId("1");
        systemEvent.setDate(new Date(System.currentTimeMillis()));
        systemEvent.setId("s1");
        systemEvent.setEvent(new Event("event2", "system event",
                new Time(System.currentTimeMillis() - (1000 * 60 * 60 * 4)), new Time(System.currentTimeMillis() - (1000 * 60 * 60 * 3)
        )));
        systemEvent.getEvent().setSystemEvent(systemEvent);
        when(systemEventService.findAllBySchoolId("1")).thenReturn(Collections.singletonList(systemEvent));

        assertEquals(eventService.findAllBySchoolIdAndYearAndMonth("1", null, null).getEventTransportList().size(), 2);
    }

    @Test
    public void findEvents_GivenSchoolIdAndYearAndMonthToFilterBy_ReturnsEventListTransportOfEventsHappeningInThatMonthOfThatYear() throws EventException {
        CustomEvent customEvent = new CustomEvent();
        customEvent.setSchoolId("1");
        customEvent.setDate(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24 * 31L)));
        customEvent.setId("c1");
        customEvent.setEvent(new Event("event1", "custom event",
                new Time(System.currentTimeMillis() - (1000 * 60 * 60 * 5)), new Time(System.currentTimeMillis() - (1000 * 60 * 60 * 4)
        )));
        customEvent.getEvent().setCustomEvent(customEvent);
        when(customEventService.findAllBySchoolIdAndDateBetween(anyString(), any(Date.class), any(Date.class))).thenReturn(new ArrayList<>());

        SystemEvent systemEvent = new SystemEvent();
        systemEvent.setSchoolId("1");
        systemEvent.setDate(new Date(System.currentTimeMillis()));
        systemEvent.setId("s1");
        systemEvent.setEvent(new Event("event2", "system event",
                new Time(System.currentTimeMillis() - (1000 * 60 * 60 * 4)), new Time(System.currentTimeMillis() - (1000 * 60 * 60 * 3)
        )));
        systemEvent.getEvent().setSystemEvent(systemEvent);
        when(systemEventService.findAllBySchoolIdAndDateBetween(anyString(), any(Date.class), any(Date.class))).thenReturn(Collections.singletonList(systemEvent));

        assertEquals(eventService.findAllBySchoolIdAndYearAndMonth("1", 2020, 7).getEventTransportList().size(), 1);
    }

    @Test(expected = EventException.class)
    public void createEvent_GivenSchoolIdAndEventTransportWithIncorrectEventType_ThrowsEventException() throws EventException {
        EventTransport eventTransport = new EventTransport();
        eventTransport.setType("MEETING");
        eventService.create(eventTransport, "1");
    }

}
