CREATE TABLE `event` (
  `id` nvarchar(36) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(256) NOT NULL,
  `starting_time` time,
  `ending_time` time,
  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `system_event` (
  `id` nvarchar(36) NOT NULL,
  `event_id` nvarchar(36) NOT NULL,
  `maintainer_id` nvarchar(36) NOT NULL,
  `school_id` nvarchar(36) NOT NULL,
  `date` date NOT NULL,

  PRIMARY KEY (`id`),
  CONSTRAINT `FK_SystemEventId` FOREIGN KEY(`event_id`) REFERENCES `event`(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `custom_event` (
  `id` nvarchar(36) NOT NULL,
  `event_id` nvarchar(36) NOT NULL,
  `school_id` nvarchar(36) NOT NULL,
  `date` date NOT NULL,

  PRIMARY KEY (`id`),
  CONSTRAINT `FK_customEventId` FOREIGN KEY(`event_id`) REFERENCES `event`(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `user_event` (
  `id`  nvarchar(36) NOT NULL,
  `event_id` nvarchar(36) NOT NULL,
  `user_id` nvarchar(36) NOT NULL,
  `school_id` nvarchar(36) NOT NULL,
  `date` date NOT NULL,

  PRIMARY KEY (`id`),
  CONSTRAINT `FK_UserEventId` FOREIGN KEY(`event_id`) REFERENCES `event`(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `meeting_event` (
  `id` nvarchar(36) NOT NULL,
  `event_id` nvarchar(36) NOT NULL,
  `parent_id` nvarchar(36) NOT NULL,
  `teacher_id` nvarchar(36) NOT NULL,   
  `meeting_id` nvarchar(36) NOT NULL,
  `school_id` nvarchar(36) NOT NULL,
  `date` date NOT NULL,

  PRIMARY KEY (`id`),
  CONSTRAINT `FK_MeetingEventId` FOREIGN KEY(`event_id`) REFERENCES `event`(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `schedule_event` (
  `id` nvarchar(36) NOT NULL,
  `event_id` nvarchar(36) NOT NULL,
  `course_id` nvarchar(36) NOT NULL,
  `school_id` nvarchar(36) NOT NULL,
  `date` date NOT NULL,

  PRIMARY KEY (`id`),
  CONSTRAINT `FK_ScheduleEventId` FOREIGN KEY(`event_id`) REFERENCES `event`(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `schedule_user`(
  `id` nvarchar(36) NOT NULL,
  `user_id` nvarchar(36) NOT NULL,
  `schedule_event_id` nvarchar(36) NOT NULL,

  PRIMARY KEY (`id`),
  CONSTRAINT `FK_ScheduleUserScheduleEventId` FOREIGN KEY(`schedule_event_id`) REFERENCES `schedule_event`(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO 