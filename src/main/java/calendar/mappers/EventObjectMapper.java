package calendar.mappers;

import calendar.entities.event.CustomEvent;
import calendar.entities.event.Event;
import calendar.entities.event.SystemEvent;
import calendar.entities.event.UserEvent;
import calendar.entities.event.external.MeetingEvent;
import calendar.entities.event.external.ScheduleEvent;
import calendar.transports.EventListTransport;
import calendar.transports.EventTransport;

import java.util.List;
import java.util.stream.Collectors;

public final class EventObjectMapper {

    private EventObjectMapper() {
    }

    public static Event toEntity(EventTransport transport) {
        Event event = new Event();

        event.setId(transport.getId());
        event.setTitle(transport.getTitle());
        event.setEndingTime(transport.getEndingTime());
        event.setDescription(transport.getDescription());
        event.setStartingTime(transport.getStartingTime());

        return event;
    }

    public static EventTransport toTransport(Event event, SystemEvent systemEvent) {
        EventTransport eventTransport = getBasicEventData(event);
        eventTransport.setSubEventId(systemEvent.getId());
        eventTransport.setRefId(systemEvent.getMaintainerId());
        eventTransport.setDate(systemEvent.getDate());
        eventTransport.setType("System");

        return eventTransport;
    }

    public static EventTransport toTransport(Event event, CustomEvent customEvent) {
        EventTransport eventTransport = getBasicEventData(event);
        eventTransport.setSubEventId(customEvent.getId());
        eventTransport.setRefId(customEvent.getSchoolId());
        eventTransport.setDate(customEvent.getDate());
        eventTransport.setType("Custom");

        return eventTransport;
    }

    public static EventTransport toTransport(Event event, MeetingEvent meetingEvent) {
        EventTransport eventTransport = getBasicEventData(event);
        eventTransport.setSubEventId(meetingEvent.getId());
        eventTransport.setRefId(meetingEvent.getSchoolId());
        eventTransport.setDate(meetingEvent.getDate());
        eventTransport.setType("Meeting");

        return eventTransport;
    }

    public static EventTransport toTransport(Event event, UserEvent userEvent) {
        EventTransport eventTransport = getBasicEventData(event);
        eventTransport.setSubEventId(userEvent.getId());
        eventTransport.setRefId(userEvent.getUserId());
        eventTransport.setDate(userEvent.getDate());
        eventTransport.setType("User");

        return eventTransport;
    }

    private static EventTransport getBasicEventData(Event event) {
        EventTransport eventTransport = new EventTransport();
        eventTransport.setId(event.getId());
        eventTransport.setTitle(event.getTitle());
        eventTransport.setEndingTime(event.getEndingTime());
        eventTransport.setDescription(event.getDescription());
        eventTransport.setStartingTime(event.getStartingTime());

        return eventTransport;
    }


    public static EventListTransport toTransport(List<Event> events) {
        EventListTransport eventListTransport = new EventListTransport();
        eventListTransport.setEventTransportList(
                events.stream().map(event -> {
                    if (event.getSystemEvent() != null) {
                        return toTransport(event, event.getSystemEvent());
                    } else if (event.getUserEvent() != null) {
                        return toTransport(event, event.getUserEvent());
                    } else if (event.getCustomEvent() != null) {
                        return toTransport(event, event.getCustomEvent());
                    } else if (event.getScheduleEvent() != null) {
                        return toTransport(event, event.getScheduleEvent());
                    }
                    return null;
                }).collect(Collectors.toList()));
        return eventListTransport;
    }

    private static EventTransport toTransport(Event event, ScheduleEvent scheduleEvent) {
        EventTransport eventTransport = getBasicEventData(event);
        eventTransport.setSubEventId(scheduleEvent.getId());
        eventTransport.setRefId(scheduleEvent.getCourseId());
        eventTransport.setDate(scheduleEvent.getDate());
        eventTransport.setType("Schedule");

        return eventTransport;
    }
}
