package calendar.transports;

import java.util.List;

public class EventListTransport {

    private List<EventTransport> eventTransportList;

    public List<EventTransport> getEventTransportList() {
        return eventTransportList;
    }

    public void setEventTransportList(List<EventTransport> eventTransportList) {
        this.eventTransportList = eventTransportList;
    }
}
