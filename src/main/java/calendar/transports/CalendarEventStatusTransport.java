package calendar.transports;

public class CalendarEventStatusTransport {

    private boolean isEmpty;

    public CalendarEventStatusTransport() {
    }

    public CalendarEventStatusTransport(boolean isEmpty) {
        this.isEmpty = isEmpty;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }
}
