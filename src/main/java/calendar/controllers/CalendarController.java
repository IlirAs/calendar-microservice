package calendar.controllers;

import calendar.entities.event.exceptions.CSVDataException;
import calendar.entities.event.exceptions.CalendarException;
import calendar.entities.event.exceptions.EventException;
import calendar.services.EventService;
import calendar.transports.CalendarEventStatusTransport;
import calendar.transports.EventListTransport;
import calendar.transports.EventTransport;
import com.itextpdf.text.DocumentException;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/calendar")
public class CalendarController {

    private final EventService eventService;

    public CalendarController(EventService eventService) {
        this.eventService = eventService;
    }

    @PostMapping("/events")
    @ResponseStatus(HttpStatus.CREATED)
    public EventTransport createEvent(@RequestBody EventTransport eventTransport,@RequestParam String schoolId) throws EventException {
        return this.eventService.create(eventTransport, schoolId);
    }

    @GetMapping("/events")
    public EventListTransport findEventsByYearAndMonth(@RequestParam String schoolId,
                                                       @RequestParam(required = false) Integer month,
                                                       @RequestParam(required = false) Integer year) throws EventException {
        return this.eventService.findAllBySchoolIdAndYearAndMonth(schoolId, year, month);
    }

    @GetMapping("/events/empty")
    public CalendarEventStatusTransport isEmpty() {
        return this.eventService.isCalendarEmpty();
    }

    @DeleteMapping("/events/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id) throws EventException {
        this.eventService.delete(id);
    }

    @PutMapping("/events")
    public EventTransport updateEvent(@RequestBody EventTransport eventTransport) throws EventException {
        return this.eventService.update(eventTransport);
    }

    @PostMapping("/upload")
    public EventListTransport importCalendarWithCsvFile(@RequestParam String schoolId, @RequestParam MultipartFile file) throws CSVDataException, CalendarException {
        return this.eventService.uploadEventsByFile(file, schoolId);
    }

    @GetMapping(value = "/download")
    public ResponseEntity<Resource> exportCalendarAsPdfFile(@RequestParam String schoolId) throws DocumentException {
        return ResponseEntity.ok().
                contentType(MediaType.APPLICATION_PDF)
                .body(this.eventService.getCalendarFile(schoolId));
    }

}
