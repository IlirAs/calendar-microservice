package calendar.util.csv;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;

import java.util.Arrays;
import java.util.List;

public class CalendarFileExportHelper {

    public static PdfPTable generatePdfTable() {
        PdfPTable table = new PdfPTable(4);
        addTableHeaders(table);
        return table;
    }

    private static void addTableHeaders(PdfPTable table) {
        List<String> titles = Arrays.asList("Title", "Date", "Starting Time", "Ending Time");
        titles.forEach(title -> {
            PdfPCell header = new PdfPCell();
            header.setBackgroundColor(BaseColor.LIGHT_GRAY);
            header.setBorderWidth(2);
            header.setPhrase(new Phrase(title));
            table.addCell(header);
        });
    }

}
