package calendar.util.csv;

import calendar.entities.event.exceptions.CSVDataException;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

public class CalendarFileImportHelper {

    public static List<String[]> extractAndValidateCSVFile(MultipartFile file) throws CSVDataException {
        List<String[]> csvValues = new ArrayList<>();
        try (CSVReader reader = new CSVReader(new InputStreamReader(file.getInputStream()))) {
            csvValues = reader.readAll();
            verifyData(csvValues);
        } catch (IOException | CsvException e) {
            e.printStackTrace();
        }
        return csvValues;
    }

    private static void verifyData(List<String[]> csvData) throws CSVDataException {
        if (csvData == null || csvData.isEmpty()) {
            throw new CSVDataException("No data provided");
        }
        if (csvData.get(0).length != 2) {
            throw new CSVDataException("Only two columns should be provided");
        }
        String firstColumnTitle = csvData.get(0)[0];
        String secondColumnTitle = csvData.get(0)[1];
        verifyColumnTitles(firstColumnTitle, secondColumnTitle);
        verifyColumnContent(csvData);

    }

    private static void verifyColumnContent(List<String[]> csvData) throws CSVDataException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        try {
            for (int i = 1; i < csvData.size(); i++) {
                if (!csvData.get(i)[0].replaceAll(" ", "").matches("^[a-zA-Z0-9]+$"))
                    throw new CSVDataException("Incorrect name format, can only contain numbers and letters");
                LocalDate.parse(csvData.get(i)[1], formatter);
            }
        } catch (DateTimeParseException e) {
            throw new CSVDataException("Incorrect date format");
        }
    }

    private static void verifyColumnTitles(String firstColumnTitle, String secondColumnTitle) throws CSVDataException {
        if (!firstColumnTitle.equalsIgnoreCase("name")
                && !firstColumnTitle.equalsIgnoreCase("date")) {
            throw new CSVDataException("Incorrect first column title");
        }
        if (!secondColumnTitle.equalsIgnoreCase("name")
                && !secondColumnTitle.equalsIgnoreCase("date")) {
            throw new CSVDataException("Incorrect second column title");
        }

        if (firstColumnTitle.equals(secondColumnTitle)) {
            throw new CSVDataException("Column titles are the same!");
        }
    }
}
