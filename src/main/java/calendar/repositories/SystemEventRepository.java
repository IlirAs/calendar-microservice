package calendar.repositories;

import calendar.entities.event.SystemEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface SystemEventRepository extends JpaRepository<SystemEvent, String> {

    List<SystemEvent> findAllBySchoolId(String schoolId);

    @Query(value = "SELECT * FROM system_event WHERE school_id=?1 AND date >= ?2 AND date <= ?3", nativeQuery = true)
    List<SystemEvent> findAllBySchoolIdAndDateBetween(String schoolId, String startingDate, String endingDate);
}
