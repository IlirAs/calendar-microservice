package calendar.repositories;

import calendar.entities.event.UserEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserEventRepository extends JpaRepository<UserEvent, String> {

    List<UserEvent> findAllByUserIdAndSchoolId(String userId, String schoolId);

    @Query(value = "SELECT * FROM user_event WHERE user_id=?1 AND school_id=?2 AND date >= ?3 AND date <= ?4", nativeQuery = true)
    List<UserEvent> findAllByUserIdAndSchoolIdAndDateBetween(String userId, String schoolId, String startingDate, String endingDate);
}
