package calendar.repositories;

import calendar.entities.event.external.MeetingEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface MeetingEventRepository extends JpaRepository<MeetingEvent, String> {

    @Query(value = "SELECT * FROM meeting_event WHERE parent_id = ?1 OR teacher_id = ?1", nativeQuery = true)
    List<MeetingEvent> findAllByParentIdOrTeacherId(String userId);

    @Query(value = "SELECT * FROM meeting_event WHERE (parent_id = ?1 OR teacher_id = ?1) AND date >= ?2 AND date <= ?3", nativeQuery = true)
    List<MeetingEvent> findAllByParentIdOrTeacherIdAndDateBetween(String userId, String startingDate, String endingDate);
}
