package calendar.repositories;

import calendar.entities.event.CustomEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomEventRepository extends JpaRepository<CustomEvent, String> {

    List<CustomEvent> findAllBySchoolId(String schoolId);

    @Query(value = "SELECT * FROM custom_event WHERE school_id=?1 AND date >= ?2 AND date <= ?3", nativeQuery = true)
    List<CustomEvent> findAllBySchoolIdAndDateBetween(String schoolId, String startingDate, String endingDate);
}
