package calendar.repositories;

import calendar.entities.event.external.ScheduleEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScheduleEventRepository extends JpaRepository<ScheduleEvent, String> {

    @Query(value = "SELECT se.id,se.event_id,se.course_id,se.school_id,se.date" +
            " FROM schedule_event se INNER JOIN schedule_user su " +
            "ON se.id=su.schedule_event_id " +
            "WHERE su.user_id=?1", nativeQuery = true)
    List<ScheduleEvent> findAllByUserId(String userId);

    @Query(value = "SELECT se.id,se.event_id,se.course_id,se.school_id,se.date" +
            " FROM schedule_event se INNER JOIN schedule_user su " +
            "ON se.id=su.schedule_event_id " +
            "WHERE su.user_id=?1 " +
            "AND se.date >= ?2 AND date <= ?3", nativeQuery = true)
    List<ScheduleEvent> findAllByUserIdAndDateBetween(String userId, String startingDate, String endingDate);

}
