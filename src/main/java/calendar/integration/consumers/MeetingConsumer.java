package calendar.integration.consumers;

import calendar.entities.event.Event;
import calendar.entities.event.external.MeetingEvent;
import calendar.integration.models.SerializableMeeting;
import calendar.services.MeetingEventService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Time;

@Service
public class MeetingConsumer {

    private final MeetingEventService meetingEventService;

    public MeetingConsumer(MeetingEventService meetingEventService) {
        this.meetingEventService = meetingEventService;
    }

    @RabbitListener(queues = {"${queue.meeting.new.online}"})
    public void handleNewOnlineMeeting(SerializableMeeting serializableMeeting) {
        Event event = new Event();
        event.setTitle(serializableMeeting.getTitle());
        event.setDescription(serializableMeeting.getDescription());
        event.setStartingTime(new Time(serializableMeeting.getStartTime()));
        event.setEndingTime(new Time(serializableMeeting.getEndTime()));

        MeetingEvent meetingEvent = new MeetingEvent();
        meetingEvent.setMeetingId(serializableMeeting.getId());
        meetingEvent.setDate(new Date(serializableMeeting.getStartTime()));
        meetingEvent.setParentId(serializableMeeting.getCreatedBy());
        meetingEvent.setTeacherId(serializableMeeting.getInvitedParticipant());
        meetingEvent.setSchoolId("");
        meetingEvent.setEvent(event);

        this.meetingEventService.create(meetingEvent);
    }

    @RabbitListener(queues = {"${queue.meeting.new.offline}"})
    public void handleNewOfflineMeeting(SerializableMeeting serializableMeeting) {
        Event event = new Event();
        event.setTitle(serializableMeeting.getTitle());
        event.setDescription(serializableMeeting.getDescription());
        event.setStartingTime(new Time(serializableMeeting.getStartTime()));
        event.setEndingTime(new Time(serializableMeeting.getEndTime()));

        MeetingEvent meetingEvent = new MeetingEvent();
        meetingEvent.setMeetingId(serializableMeeting.getId());
        meetingEvent.setParentId(serializableMeeting.getCreatedBy());
        meetingEvent.setTeacherId(serializableMeeting.getInvitedParticipant());
        meetingEvent.setSchoolId("");
        meetingEvent.setEvent(event);

        this.meetingEventService.create(meetingEvent);
    }
}