package calendar.integration.configuration;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueuesConfiguration {
    @Value("${queue.meeting.new.online}")
    private String queueMeetingNewOnline;

    @Value("${queue.meeting.new.offline}")
    private String queueMeetingNewOffline;

    //
//    @Value("${queue.user.new.parent}")
//    private String newParentsQueueName;
//
//    @Value("${queue.user.new.teacher}")
//    private String newTeachersQueueName;
//
//    @Value("${queue.user.new.admin}")
//    private String newAdminsQueueName;
//
//    @Bean(name = "emailQueue")
//    public Queue emailQueue() {
//        return new Queue(emailQueueName);
//    }
//
    @Bean(name = "newOnlineMeetingQueue")
    public Queue newOnlineMeetingQueue() {
        return new Queue(queueMeetingNewOnline);
    }

    @Bean(name = "newParentsQueue")
    public Queue newOfflineMeetingQueue() {
        return new Queue(queueMeetingNewOffline);
    }
//
//    @Bean(name = "newTeachersQueue")
//    public Queue newTeachersQueue() {
//        return new Queue(newTeachersQueueName);
//    }
//
//    @Bean(name = "newAdminsQueue")
//    public Queue newAdminsQueue() {
//        return new Queue(newAdminsQueueName);
//    }
}