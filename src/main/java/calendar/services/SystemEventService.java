package calendar.services;

import calendar.entities.event.SystemEvent;

import java.sql.Date;
import java.util.List;

public interface SystemEventService {

    List<SystemEvent> findAllBySchoolId(String schoolId);

    List<SystemEvent> findAllBySchoolIdAndDateBetween(String schoolId, Date startingDate, Date endingDate);

}
