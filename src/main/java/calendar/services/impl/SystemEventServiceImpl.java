package calendar.services.impl;


import calendar.entities.event.SystemEvent;
import calendar.repositories.SystemEventRepository;
import calendar.services.SystemEventService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class SystemEventServiceImpl implements SystemEventService {

    private final SystemEventRepository systemEventRepository;

    public SystemEventServiceImpl(SystemEventRepository systemEventRepository) {
        this.systemEventRepository = systemEventRepository;
    }

    @Override
    public List<SystemEvent> findAllBySchoolId(String schoolId) {
        return this.systemEventRepository.findAllBySchoolId(schoolId);
    }

    @Override
    public List<SystemEvent> findAllBySchoolIdAndDateBetween(String schoolId, Date startingDate, Date endingDate) {
        return this.systemEventRepository.findAllBySchoolIdAndDateBetween(schoolId, startingDate.toString(), endingDate.toString());
    }
}
