package calendar.services.impl;

import calendar.entities.event.external.MeetingEvent;
import calendar.repositories.MeetingEventRepository;
import calendar.services.MeetingEventService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class MeetingEventServiceImpl implements MeetingEventService {

    private final MeetingEventRepository meetingEventRepository;

    public MeetingEventServiceImpl(MeetingEventRepository meetingEventRepository) {
        this.meetingEventRepository = meetingEventRepository;
    }

    @Override
    public List<MeetingEvent> findAllByParentIdOrTeacherId(String userId) {
        return this.meetingEventRepository.findAllByParentIdOrTeacherId(userId);
    }

    @Override
    public List<MeetingEvent> findAllByParentIdOrTeacherIdAndDateBetween(String userId, Date startingDate, Date endingDate) {
        return this.meetingEventRepository.findAllByParentIdOrTeacherIdAndDateBetween(userId, startingDate.toString(), endingDate.toString());
    }

    @Override
    public MeetingEvent create(MeetingEvent meetingEvent) {
        return this.meetingEventRepository.save(meetingEvent);
    }

    @Override
    public void deleteById(String id) {
        this.meetingEventRepository.deleteById(id);
    }
}
