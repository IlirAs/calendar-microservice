package calendar.services.impl;

import calendar.entities.event.external.ScheduleEvent;
import calendar.repositories.ScheduleEventRepository;
import calendar.services.ScheduleEventService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class ScheduleEventServiceImpl implements ScheduleEventService {

    private final ScheduleEventRepository scheduleEventRepository;

    public ScheduleEventServiceImpl(ScheduleEventRepository scheduleEventRepository) {
        this.scheduleEventRepository = scheduleEventRepository;
    }

    @Override
    public List<ScheduleEvent> findAllByUserId(String userId) {
        return this.scheduleEventRepository.findAllByUserId("123");
    }

    @Override
    public List<ScheduleEvent> findAllByUserIdAndDateBetween(String userId, Date startingDate, Date endingDate) {
        return this.scheduleEventRepository.findAllByUserIdAndDateBetween("123", startingDate.toString(), endingDate.toString());
    }

    @Override
    public ScheduleEvent create(ScheduleEvent scheduleEvent) {
        return this.scheduleEventRepository.save(scheduleEvent);
    }

    @Override
    public void deleteById(String id) {
        this.scheduleEventRepository.deleteById(id);
    }


}
