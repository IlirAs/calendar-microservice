package calendar.services.impl;

import calendar.PerRequestIdStorage;
import calendar.entities.event.*;
import calendar.entities.event.exceptions.CSVDataException;
import calendar.entities.event.exceptions.CalendarException;
import calendar.entities.event.exceptions.EventException;
import calendar.mappers.EventObjectMapper;
import calendar.repositories.EventRepository;
import calendar.services.*;
import calendar.transports.CalendarEventStatusTransport;
import calendar.transports.EventListTransport;
import calendar.transports.EventTransport;
import calendar.util.csv.CalendarFileExportHelper;
import calendar.util.csv.CalendarFileImportHelper;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventServiceImpl implements EventService {

    private final EventRepository eventRepository;

    private final ScheduleEventService scheduleEventService;

    private final SystemEventService systemEventService;

    private final CustomEventService customEventService;

    private final MeetingEventService meetingEventService;

    private final UserEventService userEventService;

    public EventServiceImpl(EventRepository eventRepository, ScheduleEventService scheduleEventService, SystemEventService systemEventService,
                            CustomEventService customEventService, MeetingEventService meetingEventService, UserEventService userEventService) {
        this.eventRepository = eventRepository;
        this.scheduleEventService = scheduleEventService;
        this.systemEventService = systemEventService;
        this.customEventService = customEventService;
        this.meetingEventService = meetingEventService;
        this.userEventService = userEventService;
    }

    @Override
    public EventTransport create(EventTransport eventTransport, String schoolId) throws EventException {
        Event event;
        switch (eventTransport.getType().toUpperCase()) {
            case "USER":
                event = createUserEvent(eventTransport, schoolId);
                break;
            case "SYSTEM":
                event = createSystemEvent(eventTransport, schoolId);
                break;
            case "CUSTOM":
                event = createCustomEvent(eventTransport, schoolId);
                break;
            default:
                throw new EventException("Invalid event type specified");
        }
        this.eventRepository.save(event);
        return this.getEventTransportBasedOnSubEvent(event);
    }

    @Override
    public void delete(String id) throws EventException {
        this.eventRepository.deleteById(this.findOne(id).getId());
    }

    @Override
    public EventTransport update(EventTransport eventTransport) throws EventException {
        Event event = this.findOne(eventTransport.getId());
        event.setTitle(eventTransport.getTitle());
        event.setDescription(eventTransport.getDescription());
        event.setStartingTime(eventTransport.getStartingTime());
        event.setEndingTime(eventTransport.getEndingTime());

        switch (eventTransport.getType().toUpperCase()) {
            case "USER":
                return EventObjectMapper.toTransport(this.eventRepository.save(event), event.getUserEvent());
            case "SYSTEM":
                return EventObjectMapper.toTransport(this.eventRepository.save(event), event.getSystemEvent());
            case "CUSTOM":
                return EventObjectMapper.toTransport(this.eventRepository.save(event), event.getCustomEvent());
            default:
                throw new EventException("Invalid event type specified");
        }

    }

    @Override
    public EventListTransport findAllBySchoolIdAndYearAndMonth(String schoolId, Integer year, Integer month) throws EventException {
        if (year == null && month == null) {
            return EventObjectMapper.toTransport(
                    this.findAllBySchoolId(schoolId)
            );
        } else if (year != null && month != null) {
            LocalDate startingDate = LocalDate.of(year, month, 1);
            Date endingDate = Date.valueOf(LocalDate.of(year, month, startingDate.lengthOfMonth()));

            List<BaseSubEvent> subEvents = new ArrayList<>();
            subEvents.addAll(this.customEventService.findAllBySchoolIdAndDateBetween(schoolId, Date.valueOf(startingDate),
                    endingDate));
            subEvents.addAll(this.systemEventService.findAllBySchoolIdAndDateBetween(schoolId, Date.valueOf(startingDate),
                    endingDate));
            subEvents.addAll(this.scheduleEventService.findAllByUserIdAndDateBetween(PerRequestIdStorage.getUserId(), Date.valueOf(startingDate),
                    endingDate));/*userId should be accessed through token*/
            subEvents.addAll(this.meetingEventService.findAllByParentIdOrTeacherIdAndDateBetween(PerRequestIdStorage.getUserId(), Date.valueOf(startingDate),
                    endingDate));
            subEvents.addAll(this.userEventService.findAllByUserIdAndSchoolIdBetweenDate(PerRequestIdStorage.getUserId(), schoolId, Date.valueOf(startingDate),
                    endingDate));

            return EventObjectMapper.toTransport(
                    subEvents.stream().map(BaseSubEvent::getEvent).collect(Collectors.toList()));
        } else {
            throw new EventException("Incorrect request parameters");
        }
    }


    @Override
    public EventListTransport uploadEventsByFile(MultipartFile file, String schoolId) throws CSVDataException, CalendarException {
        if (!this.isCalendarEmpty().isEmpty()) {
            throw new CalendarException("Calendar is not empty!");
        }
        if (file.isEmpty()) {
            throw new CSVDataException("File is empty");
        } else if (!file.getOriginalFilename().endsWith(".csv")) {
            throw new CSVDataException("File is not of csv format");
        }
        List<String[]> csvValues = CalendarFileImportHelper.extractAndValidateCSVFile(file);
        removeColumnTitles(csvValues);

        return EventObjectMapper.toTransport(
                this.eventRepository.saveAll(parseToEvents(csvValues, schoolId))
        );
    }

    @Override
    public ByteArrayResource getCalendarFile(String schoolId) throws DocumentException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Document document = new Document();
        PdfWriter.getInstance(document, out);
        document.open();
        document.add(
                this.addRows(
                        CalendarFileExportHelper.generatePdfTable(),
                        schoolId
                )
        );
        document.close();

        return new ByteArrayResource(out.toByteArray());

    }

    @Override
    public CalendarEventStatusTransport isCalendarEmpty() {
        return new CalendarEventStatusTransport(this.eventRepository.count() == 0);
    }

    private List<Event> findAllBySchoolId(String schoolId) {
        List<BaseSubEvent> subEvents = new ArrayList<>();
        subEvents.addAll(this.customEventService.findAllBySchoolId(schoolId));
        subEvents.addAll(this.systemEventService.findAllBySchoolId(schoolId));
        subEvents.addAll(this.scheduleEventService.findAllByUserId(PerRequestIdStorage.getUserId()));
        subEvents.addAll(this.meetingEventService.findAllByParentIdOrTeacherId(PerRequestIdStorage.getUserId()));
        subEvents.addAll(this.userEventService.findAllByUserIdAndSchoolId(PerRequestIdStorage.getUserId(), schoolId));

        return subEvents.stream().map(BaseSubEvent::getEvent).collect(Collectors.toList());
    }

    private PdfPTable addRows(PdfPTable table, String schoolId) {
        this.findAllBySchoolId(schoolId).forEach(event -> {
            table.addCell(event.getTitle());
            try {
                table.addCell(this.getSubEventDate(event).toString());
            } catch (EventException e) {
                e.printStackTrace();
            }
            table.addCell(event.getStartingTime().toString());
            table.addCell(event.getEndingTime().toString());
        });
        return table;
    }

    private Date getSubEventDate(Event event) throws EventException {
        if (event.getScheduleEvent() != null) {
            return event.getScheduleEvent().getDate();
        } else if (event.getUserEvent() != null) {
            return event.getUserEvent().getDate();
        } else if (event.getSystemEvent() != null) {
            return event.getSystemEvent().getDate();
        } else if (event.getCustomEvent() != null) {
            return event.getCustomEvent().getDate();
        } else if (event.getMeetingEvent() != null) {
            return event.getMeetingEvent().getDate();
        } else {
            throw new EventException("No sub event found for event");
        }
    }


    private List<Event> parseToEvents(List<String[]> csvValues, String schoolId) {
        return csvValues.stream().map(
                data -> this.createCustomEventFromFileData(data, schoolId))
                .collect(Collectors.toList());
    }

    private Event createCustomEventFromFileData(String[] eventData, String schoolId) {
        Event event = new Event(eventData[0], "",
                Time.valueOf("00:00:00"), Time.valueOf("23:59:59"));
        CustomEvent customEvent = new CustomEvent();
        customEvent.setEvent(event);
        customEvent.setDate(Date.valueOf(LocalDate.parse(eventData[1], DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        customEvent.setSchoolId(schoolId);
        event.setCustomEvent(customEvent);

        return event;
    }

    private Event findOne(String id) throws EventException {
        return this.eventRepository.findById(id).orElseThrow(() -> new EventException("This event does not exist"));
    }

    private String[] removeColumnTitles(List<String[]> csvValues) {
        return csvValues.remove(0);
    }

    private Event createUserEvent(EventTransport eventTransport, String schoolId) {
        UserEvent userEvent = (UserEvent) setSharedData(new UserEvent(), eventTransport, schoolId);
        userEvent.setUserId(PerRequestIdStorage.getUserId());
        userEvent.getEvent().setUserEvent(userEvent);

        return userEvent.getEvent();
    }

    private BaseSubEvent setSharedData(BaseSubEvent subEvent, EventTransport eventTransport, String schoolId) {
        Event event = EventObjectMapper.toEntity(eventTransport);
        subEvent.setSchoolId(schoolId);
        subEvent.setEvent(event);
        subEvent.setDate(eventTransport.getDate());

        return subEvent;
    }

    private Event createSystemEvent(EventTransport eventTransport, String schoolId) {
        SystemEvent systemEvent = (SystemEvent) setSharedData(new SystemEvent(), eventTransport, schoolId);
        systemEvent.setMaintainerId(PerRequestIdStorage.getUserId());
        systemEvent.getEvent().setSystemEvent(systemEvent);

        return systemEvent.getEvent();
    }

    private Event createCustomEvent(EventTransport eventTransport, String schoolId) {
        CustomEvent customEvent = (CustomEvent) setSharedData(new CustomEvent(), eventTransport, schoolId);
        customEvent.setSchoolId(PerRequestIdStorage.getUserId());
        customEvent.getEvent().setCustomEvent(customEvent);

        return customEvent.getEvent();
    }


    private EventTransport getEventTransportBasedOnSubEvent(Event event) throws EventException {
        if (event.getCustomEvent() != null) {
            return EventObjectMapper.toTransport(event, event.getCustomEvent());
        } else if (event.getSystemEvent() != null) {
            return EventObjectMapper.toTransport(event, event.getSystemEvent());
        } else if (event.getUserEvent() != null) {
            return EventObjectMapper.toTransport(event, event.getUserEvent());
        } else {
            throw new EventException("This event does not have a sub event");
        }
    }
}
