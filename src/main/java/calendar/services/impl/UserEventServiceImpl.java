package calendar.services.impl;

import calendar.entities.event.UserEvent;
import calendar.repositories.UserEventRepository;
import calendar.services.UserEventService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class UserEventServiceImpl implements UserEventService {

    private final UserEventRepository userEventRepository;

    public UserEventServiceImpl(UserEventRepository userEventRepository) {
        this.userEventRepository = userEventRepository;
    }

    @Override
    public List<UserEvent> findAllByUserIdAndSchoolId(String userId, String schoolId) {
        return this.userEventRepository.findAllByUserIdAndSchoolId(userId, schoolId);
    }

    @Override
    public List<UserEvent> findAllByUserIdAndSchoolIdBetweenDate(String userId, String schoolId, Date startingDate, Date endingDate) {
        return this.userEventRepository.findAllByUserIdAndSchoolIdAndDateBetween(userId, schoolId, startingDate.toString(), endingDate.toString());
    }
}
