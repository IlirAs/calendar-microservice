package calendar.services.impl;

import calendar.entities.event.ScheduleUser;
import calendar.entities.event.exceptions.CalendarException;
import calendar.repositories.ScheduleUserRepository;
import calendar.services.ScheduleUserService;
import org.springframework.stereotype.Service;

@Service
public class ScheduleUserServiceImpl implements ScheduleUserService {


    private final ScheduleUserRepository scheduleUserRepository;

    public ScheduleUserServiceImpl(ScheduleUserRepository scheduleUserRepository) {
        this.scheduleUserRepository = scheduleUserRepository;
    }

    @Override
    public ScheduleUser findById(String id) throws CalendarException {
        return this.scheduleUserRepository.findById(id).orElseThrow(() -> new CalendarException("Incorrect id"));
    }

    @Override
    public ScheduleUser create(ScheduleUser scheduleUser) {
        return this.scheduleUserRepository.save(scheduleUser);
    }

    @Override
    public void delete(ScheduleUser scheduleUser) {
        this.scheduleUserRepository.delete(scheduleUser);
    }

    @Override
    public void deleteById(String id) {
        this.scheduleUserRepository.deleteById(id);
    }
}
