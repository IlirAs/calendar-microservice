package calendar.services.impl;

import calendar.entities.event.CustomEvent;
import calendar.repositories.CustomEventRepository;
import calendar.services.CustomEventService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class CustomEventServiceImpl implements CustomEventService {

    private final CustomEventRepository customEventRepository;

    public CustomEventServiceImpl(CustomEventRepository customEventRepository) {
        this.customEventRepository = customEventRepository;
    }

    @Override
    public List<CustomEvent> findAllBySchoolId(String schoolId) {
        return this.customEventRepository.findAllBySchoolId(schoolId);
    }

    @Override
    public List<CustomEvent> findAllBySchoolIdAndDateBetween(String schoolId, Date startingDate, Date endingDate) {
        return this.customEventRepository.findAllBySchoolIdAndDateBetween(schoolId, startingDate.toString(), endingDate.toString());
    }
}
