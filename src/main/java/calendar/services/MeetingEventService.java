package calendar.services;


import calendar.entities.event.external.MeetingEvent;

import java.sql.Date;
import java.util.List;

public interface MeetingEventService {

    List<MeetingEvent> findAllByParentIdOrTeacherId(String userId);

    List<MeetingEvent> findAllByParentIdOrTeacherIdAndDateBetween(String userId, Date startingDate, Date endingDate);

    MeetingEvent create(MeetingEvent meetingEvent);

    void deleteById(String id);

}
