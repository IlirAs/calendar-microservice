package calendar.services;


import calendar.entities.event.ScheduleUser;
import calendar.entities.event.exceptions.CalendarException;

public interface ScheduleUserService {

    ScheduleUser findById(String id) throws CalendarException;

    ScheduleUser create(ScheduleUser scheduleUser);

    void delete(ScheduleUser scheduleUser);

    void deleteById(String id);

}
