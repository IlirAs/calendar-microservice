package calendar.services;


import calendar.entities.event.CustomEvent;

import java.sql.Date;
import java.util.List;

public interface CustomEventService {

    List<CustomEvent> findAllBySchoolId(String schoolId);

    List<CustomEvent> findAllBySchoolIdAndDateBetween(String schoolId, Date startingDate, Date endingDate);

}
