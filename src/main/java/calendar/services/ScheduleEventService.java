package calendar.services;


import calendar.entities.event.external.ScheduleEvent;

import java.sql.Date;
import java.util.List;

public interface ScheduleEventService {

    List<ScheduleEvent> findAllByUserId(String userId);

    List<ScheduleEvent> findAllByUserIdAndDateBetween(String userId, Date startingDate, Date endingDate);

    ScheduleEvent create(ScheduleEvent scheduleEvent);

    void deleteById(String id);

}
