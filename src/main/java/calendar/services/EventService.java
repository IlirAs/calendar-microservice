package calendar.services;

import calendar.entities.event.exceptions.CSVDataException;
import calendar.entities.event.exceptions.CalendarException;
import calendar.entities.event.exceptions.EventException;
import calendar.transports.CalendarEventStatusTransport;
import calendar.transports.EventListTransport;
import calendar.transports.EventTransport;
import com.itextpdf.text.DocumentException;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.multipart.MultipartFile;


public interface EventService {

    void delete(String id) throws EventException;

    ByteArrayResource getCalendarFile(String schoolId) throws DocumentException;

    EventListTransport findAllBySchoolIdAndYearAndMonth(String schoolId, Integer year, Integer month) throws EventException;

    EventTransport create(EventTransport eventTransport, String schoolId) throws EventException;

    EventTransport update(EventTransport eventTransport) throws EventException;

    CalendarEventStatusTransport isCalendarEmpty();

    EventListTransport uploadEventsByFile(MultipartFile file, String schoolId) throws CSVDataException, CalendarException;
}
