package calendar.services;

import calendar.entities.event.UserEvent;

import java.sql.Date;
import java.util.List;

public interface UserEventService {

    List<UserEvent> findAllByUserIdAndSchoolId(String userId, String schoolId);

    List<UserEvent> findAllByUserIdAndSchoolIdBetweenDate(String userId, String schoolId, Date startingDate, Date endingDate);
}
