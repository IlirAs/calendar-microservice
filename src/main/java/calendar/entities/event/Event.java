package calendar.entities.event;

import calendar.entities.event.external.MeetingEvent;
import calendar.entities.event.external.ScheduleEvent;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Time;

@Entity
public class Event {

    @Id
    @Column
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column
    private String title;

    @Column
    private String description;

    @Column
    private Time startingTime;

    @Column
    private Time endingTime;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "event")
    private SystemEvent systemEvent;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "event")
    private UserEvent userEvent;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "event")
    private CustomEvent customEvent;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "event")
    private ScheduleEvent scheduleEvent;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "event")
    private MeetingEvent meetingEvent;

    public Event() {
    }

    public Event(String title, String description, Time startingTime, Time endingTime) {
        this.title = title;
        this.description = description;
        this.startingTime = startingTime;
        this.endingTime = endingTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Time getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(Time startingTime) {
        this.startingTime = startingTime;
    }

    public Time getEndingTime() {
        return endingTime;
    }

    public void setEndingTime(Time endingTime) {
        this.endingTime = endingTime;
    }

    public SystemEvent getSystemEvent() {
        return systemEvent;
    }

    public void setSystemEvent(SystemEvent systemEvent) {
        this.systemEvent = systemEvent;
    }

    public UserEvent getUserEvent() {
        return userEvent;
    }

    public void setUserEvent(UserEvent userEvent) {
        this.userEvent = userEvent;
    }

    public CustomEvent getCustomEvent() {
        return customEvent;
    }

    public void setCustomEvent(CustomEvent customEvent) {
        this.customEvent = customEvent;
    }

    public ScheduleEvent getScheduleEvent() {
        return scheduleEvent;
    }

    public void setScheduleEvent(ScheduleEvent scheduleEvent) {
        this.scheduleEvent = scheduleEvent;
    }

    public MeetingEvent getMeetingEvent() {
        return meetingEvent;
    }

    public void setMeetingEvent(MeetingEvent meetingEvent) {
        this.meetingEvent = meetingEvent;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", startingTime=" + startingTime +
                ", endingTime=" + endingTime +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Event
                && ((Event) o).getId().equals(this.getId());
    }
}
