package calendar.entities.event;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class UserEvent extends BaseSubEvent {

    @Column
    private String userId;

    public UserEvent() {
    }

    public UserEvent(String id, Event event, String userId) {
        super(id, event);
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "UserEvent{" + super.toString() +
                ", userId=" + userId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof UserEvent
                && ((UserEvent) o).getId().equals(this.getId());
    }
}
