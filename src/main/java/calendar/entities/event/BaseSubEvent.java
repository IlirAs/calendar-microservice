package calendar.entities.event;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Date;

@MappedSuperclass
public class BaseSubEvent {

    @Id
    @Column
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "event_id")
    private Event event;

    @Column
    private String schoolId;

    @Column
    private Date date;

    public BaseSubEvent() {
    }

    public BaseSubEvent(String id, Event event) {
        this.id = id;
        this.event = event;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", event=" + event +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof BaseSubEvent || ((BaseSubEvent) o).getId().equals(this.getId());
    }
}
