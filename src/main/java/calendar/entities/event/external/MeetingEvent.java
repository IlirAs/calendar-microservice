package calendar.entities.event.external;

import calendar.entities.event.BaseSubEvent;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class MeetingEvent extends BaseSubEvent {

    @Column
    private String meetingId;

    @Column
    private String parentId;

    @Column
    private String teacherId;

    public MeetingEvent() {
    }

    public String getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(String meetingId) {
        this.meetingId = meetingId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    @Override
    public String toString() {
        return "MeetingEvent{" + super.toString() +
                ", meetingId=" + meetingId +
                '}';
    }

}
