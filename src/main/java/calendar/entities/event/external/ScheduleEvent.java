package calendar.entities.event.external;

import calendar.entities.event.BaseSubEvent;
import calendar.entities.event.Event;
import calendar.entities.event.ScheduleUser;

import javax.persistence.*;
import java.util.List;


@Entity
public class ScheduleEvent extends BaseSubEvent {

    @Column
    private String courseId;

    @JoinColumn(name = "schedule_event_id")
    @OneToMany(cascade = {
            CascadeType.MERGE,
            CascadeType.REFRESH,
            CascadeType.PERSIST
    })
    private List<ScheduleUser> scheduleUsers;

    public ScheduleEvent() {
    }

    public ScheduleEvent(String id, Event event, String courseId) {
        super(id, event);
        this.courseId = courseId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public List<ScheduleUser> getScheduleUsers() {
        return scheduleUsers;
    }

    public void setScheduleUsers(List<ScheduleUser> scheduleUsers) {
        this.scheduleUsers = scheduleUsers;
    }

    @Override
    public String toString() {
        return "ScheduleEvent{" + super.toString() +
                ", courseId=" + courseId +
                '}';
    }


}

