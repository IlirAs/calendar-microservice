package calendar.entities.event.exceptions;

public class CalendarException extends Exception {
    public CalendarException(String msg) {
        super(msg);
    }
}
