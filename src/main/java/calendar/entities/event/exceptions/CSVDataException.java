package calendar.entities.event.exceptions;

public class CSVDataException extends Exception {

    public CSVDataException(String message) {
        super(message);
    }
}
