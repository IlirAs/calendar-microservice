package calendar.entities.event;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class SystemEvent extends BaseSubEvent {

    @Column
    private String maintainerId;

    public SystemEvent() {
    }

    public SystemEvent(String id, Event event, String maintainerId) {
        super(id, event);
        this.maintainerId = maintainerId;
    }

    public String getMaintainerId() {
        return maintainerId;
    }

    public void setMaintainerId(String maintainerId) {
        this.maintainerId = maintainerId;
    }

    @Override
    public String toString() {
        return "SystemEvent{" + super.toString() +
                ", maintainerId=" + maintainerId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof SystemEvent
                && ((SystemEvent) o).getId().equals(this.getId());
    }
}
